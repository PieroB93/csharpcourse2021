﻿using System.Threading.Tasks;
using System.Windows;

namespace WPFApplication
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        public string Text { get; set; }

        public MainWindow()
        {
            this.InitializeComponent();
        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            this.ManageClick();
        }

        private async Task ManageClick()
        {
            MessageBox.Show($"Hi, {this.TextBox.Text}!");
            this.Button.IsEnabled = false;
            await this.DoLongOperation();
            this.Button.IsEnabled = true;
        }

        private Task DoLongOperation()
        {
            return Task.Run(() =>
            {
                for (int i = 0; i < 10000000; i++)
                {
                    this.Dispatcher.Invoke(() => this.UpdateLabel(i));
                }
            });
        }

        private void UpdateLabel(int i)
        {
            this.Label.Content = i.ToString();
        }
    }
}