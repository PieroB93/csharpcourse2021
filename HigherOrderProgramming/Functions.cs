﻿using System;

namespace HigherOrderProgramming
{
    public class Functions
    {
        public double ExecuteOperation(int param1, int param2, Func<int, int, double> operation)
        {
            return operation(param1, param2);
            
            //ALTERNATIVE SYNTAX
            //return operation.Invoke(param1, param2);
        }
        
        public void ExecuteSamples()
        {
            double res = ExecuteOperation(6, 7, Sum);
            Console.WriteLine("First execution (Method Sum): " + res);
            
            Func<int, int, double> inlineFunction = (param1, param2) => param1 - param2;
            res = ExecuteOperation(6, 7, inlineFunction);
            Console.WriteLine("Second execution (Inline function, difference): " + res);
        }

        public double Sum(int adding1, int adding2)
        {
            return adding1 + adding2;
        }
    }
}