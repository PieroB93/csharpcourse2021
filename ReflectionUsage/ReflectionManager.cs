﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace ReflectionUsage
{
    public class ReflectionManager
    {
        public static Assembly LoadAssembly()
        {
            Assembly inheritanceAssembly = Assembly.LoadFrom(@"..\..\..\Inheritance\bin\Debug\Inheritance.exe");
            return inheritanceAssembly;
        }

        public static object InstanceClass(Assembly inheritanceAssembly, string className, params object[] parameters)
        {
            Type t = inheritanceAssembly.GetType(className);
            ConstructorInfo constructor = SelectCorrectConstructor(t, parameters);

            if (constructor == null)
            {
                throw new MissingMethodException();
            }

            return Activator.CreateInstance(t, parameters);
        }

        private static ConstructorInfo SelectCorrectConstructor(Type type, object[] actualParameters)
        {
            IEnumerable<ConstructorInfo> correctNumberOfParams =
                type.GetConstructors()
                    .Where(c => c.IsPublic)
                    .Where(c => c.GetParameters().Length == actualParameters.Length);

            if (!correctNumberOfParams.Any())
            {
                return null;
            }

            foreach (ConstructorInfo candidateConstructor in correctNumberOfParams)
            {
                ParameterInfo[] parameters = candidateConstructor.GetParameters();
                if ((parameters.Length == 0 && actualParameters.Length == 0) ||
                    actualParameters.Zip(parameters, Tuple.Create).Any(t => !t.Item1.GetType().IsSubclassOf(t.Item2.ParameterType)))
                {
                    return candidateConstructor;
                }
            }

            return null;
        }

        public static object CallMethod(object actualObject, string methodName, params object[] actualParameters)
        {
            IEnumerable<MethodInfo> correctNameAndNumberOfParams =
                actualObject.GetType().GetMethods()
                    .Where(c => c.Name == methodName)
                    .Where(c => c.IsPublic)
                    .Where(c => c.GetParameters().Length == actualParameters.Length);

            if (!correctNameAndNumberOfParams.Any())
            {
                throw new MissingMethodException();
            }

            foreach (MethodInfo candidateMethod in correctNameAndNumberOfParams)
            {
                ParameterInfo[] parameters = candidateMethod.GetParameters();
                if ((parameters.Length == 0 && actualParameters.Length == 0) ||
                    actualParameters.Zip(parameters, Tuple.Create).Any(t => !t.Item1.GetType().IsSubclassOf(t.Item2.ParameterType)))
                {
                    return candidateMethod.Invoke(actualObject, parameters);
                }
            }

            throw new MissingMethodException();
        }

        public static void PrintMethods(object person)
        {
            Type t = person.GetType();
            Console.WriteLine($"\n*** Type {t.Name} Methods ***");

            foreach (MethodInfo methods in t.GetMethods())
            {
                Console.WriteLine(methods.Name);
            }
        }

        public static void PrintTypes(Assembly assembly)
        {
            Console.WriteLine($"\n*** Assembly {assembly.GetName().Name} Types ***");
            IEnumerable<TypeInfo> types = assembly.DefinedTypes;
            foreach (TypeInfo type in types)
            {
                Console.WriteLine(type.FullName);
            }
        }

        public static void PrintAssemblyInfo(Assembly assembly)
        {
            Console.WriteLine("\n*** Assembly Info ***");
            Console.WriteLine("FullName: " + assembly.FullName);
            Console.WriteLine("Location: " + assembly.Location);
            Console.WriteLine("Codebase: " + assembly.CodeBase);
        }
    }
}