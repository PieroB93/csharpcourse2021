﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace ReflectionUsage
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            Assembly inheritanceAssembly = ReflectionManager.LoadAssembly();

            ReflectionManager.PrintAssemblyInfo(inheritanceAssembly);

            ReflectionManager.PrintTypes(inheritanceAssembly);

            object person = ReflectionManager.InstanceClass(inheritanceAssembly, "Inheritance.Person", "Piero", "Biagini", new DateTime(1993, 10, 17));

            ReflectionManager.PrintMethods(person);

            string result = (string) ReflectionManager.CallMethod(person, "GetInfos");
            Console.WriteLine("*** Result of method invocation: ***\n" + result);
            Console.ReadLine();
        }
    }
}