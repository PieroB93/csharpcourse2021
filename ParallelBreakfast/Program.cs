﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Breakfast
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            Chronometer(Program.SerialBreakfast);

            Chronometer(() =>  Program.ParallelBreakfast().Wait());

            Console.ReadLine();
        }

        public static void SerialBreakfast()
        {
            Console.WriteLine("***** Preparing Serial Breakfast *****");

            Jobs.PourCoffee();
            Jobs.FryEggs(2);
            Jobs.FryBacon(3);
            Toast toast = Jobs.ToastBread(2);
            Jobs.ApplyButter(toast);
            Jobs.ApplyJam(toast);
            Jobs.PourOrangeJuice();

            Console.WriteLine("*****  End preparing Serial Breakfast *****");
        }

        public static async Task ParallelBreakfast()
        {
            Console.WriteLine("*****  Preparing Parallel Breakfast *****");

            Coffee cup = Jobs.PourCoffee();
            Console.WriteLine("coffee is ready");

            Task<Egg> eggsTask = Jobs.FryEggsAsync(2);
            Task<Bacon> baconTask = Jobs.FryBaconAsync(3);
            Task<Toast> toastTask = Jobs.MakeToastWithButterAndJamAsync(2);

            List<Task> breakfastTasks = new List<Task> { eggsTask, baconTask, toastTask };
            while (breakfastTasks.Count > 0)
            {
                Task finishedTask = await Task.WhenAny(breakfastTasks);
                if (finishedTask == eggsTask)
                {
                    Console.WriteLine("eggs are ready");
                }
                else if (finishedTask == baconTask)
                {
                    Console.WriteLine("bacon is ready");
                }
                else if (finishedTask == toastTask)
                {
                    Console.WriteLine("toast is ready");
                }
                breakfastTasks.Remove(finishedTask);
            }

            //await Task.WhenAll(breakfastTasks);

            //await eggsTask;
            //await baconTask;
            //await toastTask;

            Juice oj = Jobs.PourOrangeJuice();
            Console.WriteLine("oj is ready");
            Console.WriteLine("Breakfast is ready!");

            Console.WriteLine("*****  End preparing Parallel Breakfast *****");
        }

        public static void Chronometer(Action act)
        {
            DateTime startTime = DateTime.Now;
            act();
            TimeSpan time = DateTime.Now - startTime;
            Console.WriteLine("Took: " + time);
        }
    }
}