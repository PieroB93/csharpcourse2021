﻿using System;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            this.InitializeComponent();
        }

        private void Greet_Click(object sender, EventArgs e)
        {
            this.ShowGreetMessage();
        }

        private async Task ShowGreetMessage()
        {
            MessageBox.Show($"Hi, {this.TextBox.Text}!");
            this.button1.Enabled = false;
            await this.DoLongOperation();
            this.button1.Enabled = true;
        }

        private Task DoLongOperation()
        {
            return Task.Run(() =>
            {
                for (int i = 0; i < 10000000; i++)
                {
                    this.Invoke(
                        new Action(() => this.label1.Text = i.ToString())
                        );
                }
            });
        }
    }
}