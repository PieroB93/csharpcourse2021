﻿namespace Generics
{
    public interface IItemValue
    {
        string GetStringedValue();
    }
}