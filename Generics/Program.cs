﻿using System;

namespace Generics
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            BuildLinkedList();
        }

        private static void BuildLinkedList()
        {
            LinkedListItem<IntegerValue> head = new LinkedListItem<IntegerValue>(new IntegerValue(-1));
            //COULD USE IntegerValue or StringValue? Yes
            //COULD USE OTHER TYPE, E.G: string? No!!

            LinkedListItem<IntegerValue> currentItem = head;
            for (int i = 0; i < 5; i++)
            {
                currentItem.Child = new LinkedListItem<IntegerValue>(new IntegerValue(i));
                currentItem = currentItem.Child;
            }

            currentItem = head;
            while (currentItem.Child != null)
            {
                Console.WriteLine("Value: " + currentItem.Child.Value.GetStringedValue());
                currentItem = currentItem.Child;
            }

            Console.ReadLine();
        }
    }
}