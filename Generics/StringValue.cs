﻿namespace Generics
{
    public class StringValue : IGenericItemValue<string>
    {
        private string _value;
        
        public StringValue(string i)
        {
            _value = i;
        }

        public string Value { get; set; }

        public string GetStringedValue()
        {
            return _value;
        }
    }
}