﻿using System;

namespace Generics
{
    public class LinkedListItem<TType> where TType : IItemValue
    {
        /// <summary>
        /// Next element
        /// </summary>
        public LinkedListItem<TType> Child { get; set; }
        
        /// <summary>
        /// Current item value
        /// </summary>
        public TType Value { get; }

        public LinkedListItem(TType value)
        {
            this.Value = value;
        }

        public LinkedListItem(LinkedListItem<TType> child, TType value)
        {
            this.Child = child;
            this.Value = value;
        }
    }
}