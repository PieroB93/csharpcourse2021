﻿namespace Generics
{
    public interface IGenericItemValue<T> : IItemValue
    {
        T Value { get; set; }
    }
}