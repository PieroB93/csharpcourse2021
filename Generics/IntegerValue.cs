﻿namespace Generics
{
    public class IntegerValue : IGenericItemValue<int>
    {
        private int _value;
        
        public IntegerValue(int i)
        {
            _value = i;
        }

        public int Value { get; set; }

        public string GetStringedValue()
        {
            return "" + _value;
        }
    }
}