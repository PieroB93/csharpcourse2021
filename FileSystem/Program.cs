﻿using System;
using System.IO;
using System.Linq;

namespace FileSystem
{
    internal class Program
    {
        private static string _tempFolder;
        private static readonly string _tempFolderName = "fileAndFolderManipulation";
        private static readonly string _tempFileName = "sampleFile";

        public static void Main(string[] args)
        {
            _tempFolder = Path.Combine(Path.GetTempPath(), _tempFolderName);

            FolderManipulation();
            FileManipulation();
            PathManipulation();

            DeleteAll();
        }

        private static void DeleteAll()
        {
            Directory.Delete(Program._tempFolder, true);
            Console.WriteLine($"Directory exists? {Directory.Exists(_tempFolder)}");
        }

        private static void FileManipulation()
        {
            Console.WriteLine("\n\n*** File operations ***");

            string sampleFile = Path.Combine(_tempFolder, _tempFileName);
            string sampleFileCopy = Path.Combine(_tempFolder, _tempFileName + "-copy");

            Console.WriteLine("\nFile names:");
            Console.WriteLine("- SampleFile: " + sampleFile);
            Console.WriteLine("- SampleFileCopy: " + sampleFileCopy);

            Console.WriteLine($"\nFile exists? {File.Exists(sampleFile)}");

            Console.WriteLine("\nCreating sampleFile");
            using(File.Create(sampleFile))
            {
                ; //Using because we need to close the file!
            }

            Console.WriteLine($"File exists? {File.Exists(sampleFile)}");

            Console.WriteLine("\nDeleting sampleFile");
            File.Delete(sampleFile);
            Console.WriteLine($"File exists? {File.Exists(sampleFile)}");

            Program.ReadingAndWritingFileInEasyMode(sampleFile, sampleFileCopy);
            Program.ReadingAndWritingFileInFullMode(sampleFile, sampleFileCopy);

            Console.WriteLine("\nCopying sampleFile in sampleFileCopy");
            File.Copy(sampleFile, sampleFileCopy);

            Console.WriteLine("\nReading sampleFileCopy");
            string content = File.ReadAllText(sampleFileCopy);
            Console.WriteLine(content);

            Console.WriteLine("\nDeleting sampleFileCopy");
            File.Delete(sampleFileCopy);
            Console.WriteLine($"File exists? {File.Exists(sampleFileCopy)}");

            Console.WriteLine("\nMoving sampleFile in sampleFileCopy");
            File.Move(sampleFile, sampleFileCopy);
            Console.WriteLine($"sampleFile exists? {File.Exists(sampleFile)}");
            Console.WriteLine($"sampleFileCopy exists? {File.Exists(sampleFileCopy)}");
        }

        private static void PathManipulation()
        {
            Console.WriteLine("\n\n*** Path Manipulation ***");
            string sampleFilePath = Path.Combine(Program._tempFolder, Path.GetRandomFileName());

            string directoryName = Path.GetDirectoryName(sampleFilePath);
            string fileName = Path.GetFileName(sampleFilePath);
            string fileNameWithoutExtension = Path.GetFileNameWithoutExtension(sampleFilePath);
            string extension = Path.GetExtension(sampleFilePath);

            Console.WriteLine("Full path: " + sampleFilePath);
            Console.WriteLine("Directory Name: " + directoryName);
            Console.WriteLine("File Name: " + fileName);
            Console.WriteLine("File Name Without Extension: " + fileNameWithoutExtension);
            Console.WriteLine("Extension: " + extension);
        }

        private static void FolderManipulation()
        {
            Console.WriteLine("\n\n*** Directory operations ***");

            Console.WriteLine("Directory name: " + _tempFolder);

            Console.WriteLine($"\nDirectory exists? {Directory.Exists(_tempFolder)}");

            Console.WriteLine("\nCreating directory");
            Directory.CreateDirectory(_tempFolder);

            Console.WriteLine($"Directory exists? {Directory.Exists(_tempFolder)}");
        }

        private static void ReadingAndWritingFileInEasyMode(string sampleFile, string sampleFileCopy)
        {
            Console.WriteLine("\nWriting into sampleFile");
            File.WriteAllText(sampleFile, "This is the content of the sample file\nThe content is split in two lines!");

            Console.WriteLine("\nReading sampleFile");
            string content = File.ReadAllText(sampleFile);
            Console.WriteLine(content);

            Console.WriteLine("\nReading sampleFile by rows");
            string[] contentByRow = File.ReadAllLines(sampleFile);
            Console.WriteLine("Rows read: " + contentByRow);
        }

        private static void ReadingAndWritingFileInFullMode(string sampleFile, string sampleFileCopy)
        {
            Console.WriteLine("\nWriting into sampleFile");
            WriteToFileFullMode(sampleFile, "This is the content of the sample file\nThe content is split in two lines!");

            Console.WriteLine("\nReading sampleFile");
            string content = Program.ReadFromFileFullMode(sampleFile);
            Console.WriteLine(content);

            Console.WriteLine("\nReading sampleFile by rows");
            string[] contentByRow = Program.ReadFromFileByLinesFullMode(sampleFile);
            Console.WriteLine("Rows read: " + contentByRow);
        }

        private static void WriteToFileFullMode(string sampleFile, string content)
        {
            using FileStream fs = new FileStream(sampleFile, FileMode.OpenOrCreate, FileAccess.Write);
            using StreamWriter sr = new StreamWriter(fs);

            sr.Write(content);
        }

        private static string ReadFromFileFullMode(string sampleFile)
        {
            using FileStream fs = new FileStream(sampleFile, FileMode.OpenOrCreate, FileAccess.Read);
            using StreamReader sr = new StreamReader(fs);

            return sr.ReadToEnd();
        }

        private static string[] ReadFromFileByLinesFullMode(string sampleFile)
        {
            using FileStream fs = new FileStream(sampleFile, FileMode.OpenOrCreate, FileAccess.Read);
            using StreamReader sr = new StreamReader(fs);

            string[] fileContent = Array.Empty<string>();
            while (!sr.EndOfStream)
            {
                fileContent = fileContent.Append(sr.ReadLine()).ToArray();
            }

            return fileContent;
        }
    }
}