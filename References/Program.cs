﻿using Inheritance;
using Newtonsoft.Json;
using System;

namespace References
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            Person p = new Person("Piero", "Biagini", new DateTime(1993, 10, 17));
            Console.WriteLine("\n*** Not serialized person:\n" + p);

            string serialized = JsonConvert.SerializeObject(p);
            Console.WriteLine("\n*** Serialized person:\n" + serialized);

            Person deserialized = (Person) JsonConvert.DeserializeObject(serialized, typeof(Person));
            Console.WriteLine("\n*** Deserialized person:\n" + deserialized);
            Console.ReadLine();
        }
    }
}