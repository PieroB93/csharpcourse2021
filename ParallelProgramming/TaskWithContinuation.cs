﻿using System;
using System.Threading.Tasks;

namespace ParallelProgramming
{
    public class TaskWithContinuation
    {
        public async Task DoSample()
        {
            Task<string> task = Task.Run(
                () =>
                {
                    DateTime date = DateTime.Now;
                    return date.Hour > 17
                        ? "evening"
                        : date.Hour > 12
                            ? "afternoon"
                            : "morning";
                });
        
            await task.ContinueWith(
                antecedent =>
                {
                    if (task.Status == TaskStatus.RanToCompletion)
                    {
                        Console.WriteLine($"Good {antecedent.Result}!");
                        Console.WriteLine($"And how are you this fine {antecedent.Result}?");
                    }
                    else
                    {
                        Console.WriteLine("ERROR while executing the first task");
                    }
                });
        }
}
}