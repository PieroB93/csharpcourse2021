﻿using System;
using System.Threading.Tasks;

namespace ParallelProgramming
{
    internal class Program
    {
        private static async Task Main(string[] args)
        {
            // new ThreadSamples().DoSample();
            // new SimpleTaskSample().DoSampleWithReturn();
            //new TaskWithContinuation().DoSample();
            new TaskWithCancellationTokenSample().DoSample();

            Console.ReadLine();
        }
    }
}
