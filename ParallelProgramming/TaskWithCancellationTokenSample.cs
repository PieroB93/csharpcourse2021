﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace ParallelProgramming
{
    public class TaskWithCancellationTokenSample
    {
        public void DoSample()
        {
            CancellationTokenSource cts = new CancellationTokenSource();
            CancellationToken ct = cts.Token;
            Task t = Task.Run(() => this.CancellableParallelOperation(ct), ct);
            Console.WriteLine("Task started. State: " + t.Status);

            this.MainThreadOperation();
            Console.WriteLine("Main operation ended");

            Console.WriteLine("Cancelling task");
            cts.Cancel();
            Console.WriteLine("Task cancelled. State: " + t.Status);
            Console.WriteLine("\tCompleted: " + t.IsCompleted);
            
            Console.ReadLine();
        }


        public void MainThreadOperation()
        {
            for (int i = 0; i < 50; i++)
            {
                Console.WriteLine("MainThread operation - iteration " + i);
                Thread.Sleep(100);
                i++;
            }
        }

        public void CancellableParallelOperation(CancellationToken ct)
        {
            //WHAT IF ALREADY CANCELLED?
            ct.ThrowIfCancellationRequested();
            
            for (int i = 0; i < 100; i++)
            {
                Console.WriteLine("Parallel operation - iteration "+ i);
                Thread.Sleep(100);
                ct.ThrowIfCancellationRequested();
            }
        }
    }
}