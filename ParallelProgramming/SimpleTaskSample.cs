﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace ParallelProgramming
{
    public class SimpleTaskSample
    {
        public void DoSample()
        {
            Task t = Task.Run(this.ParallelOperation);
            Console.WriteLine("Task started. State: " + t.Status);

            this.MainThreadOperation();
            Console.WriteLine("Main operation ended");

            Console.WriteLine("Waiting thread end...");
            t.Wait();
            Console.WriteLine("Task ended. State: " + t.Status);
            Console.ReadLine();
        }

        public void DoSample2nd()
        {
            Task t = new Task(this.ParallelOperation);
            Console.WriteLine("Task created. State: " + t.Status);
            
            t.Start();
            Console.WriteLine("Task started. State: " + t.Status);

            this.MainThreadOperation();
            Console.WriteLine("Main operation ended");

            Console.WriteLine("Waiting thread end...");
            t.Wait();
            Console.WriteLine("Task ended. State: " + t.Status);
            Console.ReadLine();
        }
        
        public void DoSampleWithReturn()
        {
            Task<int> t = new Task<int>(this.ParallelOperationWithReturn);
            Console.WriteLine("Task created. State: " + t.Status);
            
            t.Start();
            Console.WriteLine("Task started. State: " + t.Status);

            this.MainThreadOperation();
            Console.WriteLine("Main operation ended");

            Console.WriteLine("Waiting thread end...");
            t.Wait();
            Console.WriteLine("Task ended. State: " + t.Status);
            Console.WriteLine("\tResult: " + t.Result);
            Console.ReadLine();
        }


        public void MainThreadOperation()
        {
            for (int i = 0; i < 100; i++)
            {
                Console.WriteLine("MainThread operation - iteration " + i);
                Thread.Sleep(100);
                i++;
            }
        }

        public void ParallelOperation()
        {
            for (int i = 0; i < 200; i++)
            {
                Console.WriteLine("Parallel operation - iteration "+ i);
                Thread.Sleep(100);
            }
        }
        
        public int ParallelOperationWithReturn()
        {
            for (int i = 0; i < 200; i++)
            {
                Console.WriteLine("Parallel operation with return - iteration "+ i);
                Thread.Sleep(100);
            }

            return 0;
        }
    }
}