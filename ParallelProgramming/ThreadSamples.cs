﻿using System;
using System.Threading;

namespace ParallelProgramming
{
    public class ThreadSamples
    {
        public void DoSample()
        {
            Thread t = new Thread(this.ParallelOperation)
            {
                Name = "ParallelThread"
            };
            Console.WriteLine("Thread " + t.Name + " created. State: " + t.ThreadState);

            t.Start();
            Console.WriteLine("Thread " + t.Name + " started. State: " + t.ThreadState);

            this.MainThreadOperation();
            Console.WriteLine("Main operation ended");

            Console.WriteLine("Waiting thread end...");
            t.Join();
            Console.WriteLine("Thread " + t.Name + " ended. State: " + t.ThreadState);
            Console.ReadLine();

        }

        public void MainThreadOperation()
        {
            for (int i = 0; i < 100; i++)
            {
                Console.WriteLine("MainThread operation - iteration " + i);
                Thread.Sleep(100);
                i++;
            }
        }

        public void ParallelOperation()
        {
            for (int i = 0; i < 200; i++)
            {
                Console.WriteLine("Parallel operation - iteration "+ i);
                Thread.Sleep(100);
            }
        }
    }
}
