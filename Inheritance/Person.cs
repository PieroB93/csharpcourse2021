﻿using System;

namespace Inheritance
{
    /// <summary>
    /// Modella una generica persona
    /// </summary>
    public class Person : IPerson
    {
        public Person(string name, string lastName, DateTime birthDate)
        {
            this.Name = name;
            this.LastName = lastName;
            this.BirthDate = birthDate;
        }

        /// <summary>
        /// Nome
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Cognome
        /// </summary>
        public string LastName { get; set; }

        private DateTime _birthDateTime;

        /// <summary>
        /// Data di nascita
        /// </summary>
        public DateTime BirthDate
        {
            get => this._birthDateTime;
            set
            {
                if (value < new DateTime(1900, 01, 01))
                {
                    throw new ArgumentOutOfRangeException("Too low date!");
                }
                else if (value > DateTime.Now)
                {
                    throw new ArgumentOutOfRangeException("Future date!");
                }

                this._birthDateTime = value;
            }
        }

        public virtual string GetInfos()
        {
            return "----\n" +
                   this.Name + " " + this.LastName + "\n" +
                   this.BirthDate.ToString("d");
        }

        public override string ToString()
        {
            return this.GetType().FullName + "[" + this.Name + " " + this.LastName + "]";
        }
    }
}