﻿using System;

namespace Inheritance
{
    /// <summary>
    /// Modella una generica persona
    /// </summary>
    public interface IPerson
    {
        /// <summary>
        /// Nome
        /// </summary>
        string Name { get; set; }

        /// <summary>
        /// Cognome
        /// </summary>
        string LastName { get; set; }

        /// <summary>
        /// Data di nascita
        /// </summary>
        DateTime BirthDate { get; set; }

        /// <summary>
        /// Ottiene le informazioni generiche di una persona
        /// </summary>
        string GetInfos();
    }
}