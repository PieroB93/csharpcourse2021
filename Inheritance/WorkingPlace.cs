﻿namespace Inheritance
{
    public enum WorkingPlace
    {
        Ancona,
        Bologna,
        Brescia,
        Catanzaro,
        Cesena,
        Lecco,
        Milano,
        Padova,
        Roma,
        Torino
    }
}