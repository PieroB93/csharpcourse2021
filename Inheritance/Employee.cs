﻿using System;

namespace Inheritance
{
    public class Employee : Person
    {
        public WorkingPlace WorkingPlace { get; set; }
        
        public Employee(string name, string lastName, DateTime birthDate, WorkingPlace workingPlace) : base(name, lastName, birthDate)
        {
            this.WorkingPlace = workingPlace;
        }
        
        
    }
}
