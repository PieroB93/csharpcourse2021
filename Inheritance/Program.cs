﻿using System;

namespace Inheritance
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Person person = new Person("Piero", "Biagini", DateTime.Today);
            Employee employee = new Employee("Piero", "Biagini", DateTime.Today, WorkingPlace.Cesena);

            IPerson iPerson = employee;

            Employee employee2nd = (Employee) iPerson;

        }
    }
}
