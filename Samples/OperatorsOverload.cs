﻿using System;

namespace Samples
{
    public class OperatorsOverload
    {
        public string Value { get; }

        public OperatorsOverload(string value)
        {
            this.Value = value;
        }

        public static OperatorsOverload operator +(OperatorsOverload a, OperatorsOverload b)
        {
            return new OperatorsOverload(a.Value + b.Value);
        }

        public static void Sample()
        {
            OperatorsOverload first = new OperatorsOverload("Hello, ");
            OperatorsOverload second = new OperatorsOverload("world!");
            OperatorsOverload sum = first + second;
            Console.WriteLine(sum.Value);

        }
    }
}