﻿using System.Text;

namespace Samples
{
    public class Strings
    {
        public void StringCreation()
        {
            int aVariable = 0;
            
            string string1 = "This is a string created by assignment.";
            string string2 = $"The value of aVariable is {aVariable}";
            string string3 = string.Format("The value of aVariable is {0}", aVariable);  

            char[] chars = { 'w', 'o', 'r', 'd' };
            string string4 = new string(chars);

            string string5 = new string('c', 20);
        }

        public void StringBuilder()
        {
            string aString = "this is a string";
            string anotherString = "this is another string";
            
            StringBuilder sb = new StringBuilder();
            sb.Append(aString);
            sb.Append(anotherString);
            string string6 = sb.ToString();
        }
    }
}