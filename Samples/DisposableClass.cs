﻿using System;

namespace Samples
{
    public class DisposableClass : IDisposable
    {
        //OTHER METHODS AND FIELDS
        
        public void Dispose()
        {
            //FREE HERE NON MANAGED RESOURCES
        }
    }
}