﻿using System.IO;

namespace Samples
{
    public class Using
    {
        public void BlockUsingSample()
        {
            using (MemoryStream disposableClass = new MemoryStream())
            {
                //USE THE DISPOSABLE CLASS
            }
            //BY THE END OF THE BLOCK, THE OBJECT IS DISPOSED
        }

        public void InlineUsingSample()
        {
            using MemoryStream disposableClass = new MemoryStream();
            //USE THE DISPOSABLE CLASS
            //BY THE END OF THE BLOCK, THE OBJECT IS DISPOSED
        }
    }
}