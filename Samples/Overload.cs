﻿using System;

namespace Samples
{
    public class Overload
    {
        public double Diff(float a, float b)
        {
            return a - b;
        }

        public int Diff(int a, int b)
        {
            return a - b;
        }

        public TimeSpan Diff(DateTime a, DateTime b)
        {
            return a - b;
        }
    }
}