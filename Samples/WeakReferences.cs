﻿using System;

namespace Samples
{
    public class WeakReferences
    {
        public void WeakReferenceSamples()
        {
            string anObject = "I'm an object";

            WeakReference wref = new WeakReference(anObject);
            if (wref.IsAlive)
            {
                //CAN USE THE OBJECT
                string myObject = wref.Target as string;
            }
        }
    }
}