﻿using System;

namespace Samples
{
    public class Syntax
    {
        public void SelectionSamples()
        {
            bool booleanCondition = false;

            if (booleanCondition)
            {
                //DO SOMETHING
            }
            else
            {
                //DO SOMETHING OTHER
            }

            string valueType = "a";

            switch (valueType)
            {
                case "a":
                case "b":
                    Console.Write("test1");
                    break;
                case "c":
                    //DO SOMETHING
                    break;
                default:
                    //DO SOMETHING
                    break;
            }

            int res = valueType switch
                {
                    "a" => 1,
                    "b" => 2,
                    _ => 0
                };
        }

        public void IterationSamples()
        {
            int numOfIterations = 5;
            bool condition = false;

            for (int i = 0; i < numOfIterations; i++)
            {
                //DO SOMETHING HERE
            }

            while (condition)
            {
                //DO SOMETHING HERE
            }

            do
            {
                //DO SOMETHING HERE
            } while (condition);

            int[] array = new int[] {0, 1, 2, 3};
            
            foreach(int item in array)
            {
                //DO SOMETHING HERE
            }
        }

        public void TryCatchSamples()
        {
            try
            {
                //SOMETHING THAT THROWS EXCEPTION
            }
            catch (Exception e)
            {
                //ERROR MANAGEMENT
            }
            finally
            {
                //SOMETHING THAT SHOULD BE ALWAYS EXECUTED
            }
        }

        public void ArraySamples()
        {
            int[] arrayOfInts = new int[] {1, 2, 3, 4};
            string[] arrayOfStrings = new string[10];
            int[,] multiDimArray = new int[10, 20];
        }
    }
}