﻿using System;

namespace Samples
{
    public class Tuples
    {
        public void Sample()
        {
            //UNNAMED TUPLE: CREATION AND ACCESS
            Tuple<string, int> unnamedTuple = new Tuple<string, int>("string", 0);
            string stringPart = unnamedTuple.Item1;
            int intPart = unnamedTuple.Item2;
            
            //NAMED TUPLE: CREATION AND ACCESS
            (string stringPart, int intPart) namedTuple = ("string", 0);
            stringPart = namedTuple.stringPart;
            intPart = namedTuple.intPart;
        }
    }
}