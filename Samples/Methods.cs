﻿namespace Samples
{
    public interface Methods
    {
        void MyOptionalMethod(string first, string second, string third = "");

        void MyVariableMethod(params string[] args);

        void MyInOutRefMethod(in string inParam, out string outParam, ref string refParam);

    }

    public class MethodsImpl : Methods
    {
        public void MyOptionalMethod(string first, string second, int third = 0, string fourth = "asdf")
        {

        }

        public void MyOptionalMethod(string first, string second, string fourth)
        {

        }

        public void MyVariableMethod(params string[] args)
        {

        }

        public void MyInOutRefMethod(in string inParam, out string outParam, ref string refParam)
        {
            outParam = "out";
            refParam = "ref1";
        }


        private void test()
        {
            this.MyOptionalMethod("first", "second", 0, "third");
            this.MyOptionalMethod("first", "second", "test");

            this.MyVariableMethod();

            string refParam = "ref";
            this.MyInOutRefMethod("in", out string outParam, ref refParam);
        }
    }
}