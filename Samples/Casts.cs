﻿namespace Samples
{
    public interface IBaseClass
    {

    }

    public class BaseClass : IBaseClass
    {

    }

    public class DerivedClass : BaseClass
    {

    }

    public class Casts
    {
        public void NumericImplicitCast()
        {
            int num = 100;
            long bigNum = num;
            double decimalNum = num;
        }

        public void BaseClassImplicitCast()
        {
            DerivedClass derivedClass = new DerivedClass();
            BaseClass baseClass = derivedClass;
            IBaseClass iBaseClass = derivedClass;
        }

        public void ExplicitCast()
        {
            BaseClass myObject = new DerivedClass();
            DerivedClass myCastedObject = (DerivedClass) myObject;
        }

        public void TypeOperators()
        {
            IBaseClass anObject = new DerivedClass();

            bool test = anObject is DerivedClass; //TRUE
        }
    }
}