﻿using System;

namespace Samples
{
    public class Nullables
    {
        public void NullableSamples()
        {
            int? nullableInt = null;
            bool hasValue = nullableInt.HasValue; //FALSE

            try
            {
                int valueWithException = nullableInt.Value;
            }
            catch (InvalidOperationException e)
            {
                //ERROR: nullableInt HAS NOT A VALUE!
            }

            int tryGetValue = nullableInt.GetValueOrDefault(); //0

            nullableInt = 5;
            hasValue = nullableInt.HasValue; //TRUE

            int value = nullableInt.Value;
            //NOT ERROR: nullableInt HAS VALUE
        }
    }
}