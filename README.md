# README #
Repository creato per il corso ***C# Language***, indetto all'interno del piano *Fondo Nuove Competenze* a maggio 2021 da *Formula-Impresoft*.


## Agenda del corso
* 10/05/2021, 9.00-13.00
* 11/05/2021, 9.00-13.00
* 13/05/2021, 9.00-13.00
* 14/05/2021, 9.00-13.00

## Argomenti
1. **Introduzione**
     * Caratteristiche
     * Documentazione
     * Sintassi di base
       * Dichiarazioni ed assegnamenti
       * Operatori
       * Commenti e documentazione
     * Code Conventions
     * IDE
     * organizzazione del codice
1. **C# e programmazione a oggetti**
     * Tipi di dato: tipi valore, tipi riferimenti e nullables
     * Uguaglianza tra oggetti
     * Classi, interfacce e classi astratte
     * Ereditarietà
     * Costruttori
     * Metodi
       * Overload
       * Override
       * Parametri particolari 
       * Metodi di Object
     * Modificatori d'accesso
     * Membri statici e membri d'istanza
     * Proprietà
     * Sintassi di accesso
1. **Esempi pratici**
     * Hello, world
     * Esecuzione e debugging
     * Ereditarietà
1. **Elementi basici del linguaggio C#**
     * Strutture di selezione (if, switch, ...)
     * Array
     * Strutture iterative (for, while, ...)
     * Eccezioni
     * Stringhe
     * Cast e operatori di tipo
1. **Gestione della memoria**
     * Garbage Collector
     * IDisposable
     * Using
     * Weak Reference
1. **Interazione con il File System**
     * Manipolazione di file e cartelle
     * Lettura e scrittura di file
1. **Metodi di estensione**
     * Cosa sono
     * Come si usano
1. **Tipi generici**
     * Tipi generici
     * Costruzione di una classe
     * Creazione di un oggetto
     * Covarianza e controvarianza
1. **Programmazione Higher Order**
     * Concetto di Higher Order
     * I Delegate
     * Le Func
     * Le Action
1. **Le Tuple**
     * Cosa sogno
     * Creazione ed accesso
1. **Le Collections**
     * L'interfaccia ICollection\<T>
     * Operazioni fondamentali
     * Tipi fondamentali di collections: 
       * List
       * Set
       * Dictionary
     * Enumerable
       * Interfaccia
       * Funzionamento e performance
       * Creazione
       * Metodi principali
         * Select e SelectMany
         * Where e OfType
         * First, Single, Last, ElementAt
         * ...
  
Altri temi ancora non sviluppati:
* Programmazione parallela
* Eventi
* LINQ