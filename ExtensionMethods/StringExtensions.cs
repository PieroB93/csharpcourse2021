﻿namespace ExtensionMethods
{
    public static class StringExtensions
    {
        public static string ReplaceA(this string originalString, string newValue)
        {
            return originalString.Replace("a", newValue);
        }

        public static void SampleUsage()
        {
            string res = StringExtensions.ReplaceA("this is a string", "b");
            //"this is b string"

            res = "this is a string".ReplaceA("b");
            //"this is b string"
        }

        public static void Main(string[] args)
        {
            StringExtensions.SampleUsage();

        }
    }
}