﻿namespace ParallelProgramming
{
    internal class Program
    {
        public class MyClass
        {
            public int myValue;
        }

        private static void Increment(ref int i)
        {
            i++;
        }

        private static void Increment(MyClass obj)
        {
            obj.myValue = obj.myValue + 1;
        }

        private static void Initializer(MyClass t)
        {
            t = new MyClass();
        }

        private static void Main(string[] args)
        {
            MyClass cls = new MyClass();
            object cls1 = new MyClass();

            MyClass asd;

            MyClass cls2 = new MyClass();

            asd = cls2;
        }


        public int Increment(int i)
        {
            return i++;
        }
    }
}
