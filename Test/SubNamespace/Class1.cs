﻿using System;

namespace Test.SubNamespace
{
    public class Class1
    {
        public static void Test()
        {
            MyEnum val3 = MyEnum.Value3 | MyEnum.Value1;
            val3.HasFlag(MyEnum.Value3);
            val3.HasFlag(MyEnum.Value1);
            val3.HasFlag(MyEnum.Value2);
            val3 = val3 | MyEnum.Value2;
        }

        [Flags]
        public enum MyEnum
        {
            Value1 = 2,
            Value2 = 4,
            Value3 = 8
        }
    }

    
}
