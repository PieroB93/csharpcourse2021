﻿using System.Collections.Generic;

namespace Collections
{
    public class CollectionTypes
    {
        public void DoSample()
        {
            IList<string> listOfStrings =  new List<string>();

            for (int i = 0; i < 5; i++)
            {
                listOfStrings.Add("Elemento " + i);
            }

            ISet<int> setOfInts = new HashSet<int>();
            
            for (int i = 0; i < 5; i++)
            {
                setOfInts.Add(i);
            }

            setOfInts.Add(1);
        }
    }
}