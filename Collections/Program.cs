﻿using System.Collections.Generic;
using Generics;

namespace Collections
{
    public class Program
    {
        public static void Main(string[] args)
        {
            new CollectionTypes().DoSample();
            new Enumerables().DoSample();
        }
    }
}