﻿using Generics;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Collections
{
    public class LinqSamples
    {
        public void DoSample()
        {
            IEnumerable<IntegerValue> myEnumerable = this.CreateEnumerable();

            this.PrintEnumerable(myEnumerable, nameof(myEnumerable));

            Console.WriteLine("LINQ QUERY");
            IEnumerable<IntegerValue> sampleLinqQuery =
                from item in myEnumerable
                where item.Value > 3
                select item;

            Console.WriteLine("SELECT");
            IEnumerable<IntegerValue> selectedEnumerable =
                from item2 in myEnumerable
                select new IntegerValue(item2.Value + 1);

            this.PrintEnumerable(myEnumerable, nameof(myEnumerable));
            this.PrintEnumerable(selectedEnumerable, nameof(selectedEnumerable));

            Console.WriteLine("WHERE");
            IEnumerable<IntegerValue> whereEnumerable =
                from item3 in myEnumerable
                where item3.Value > 5
                select item3;

            this.PrintEnumerable(whereEnumerable, nameof(whereEnumerable));

            Console.WriteLine("GROUP BY");
            IEnumerable<IGrouping<bool, IntegerValue>> groupedByEnumerable =
                from item4 in myEnumerable
                group item4 by ((item4.Value % 2) == 0);
        }

        private void PrintEnumerable(IEnumerable<string> enumerable, string enumerableName)
        {
            Console.WriteLine("----");
            Console.WriteLine("Enumerable name: " + enumerableName);
            foreach (string i in enumerable)
            {
                Console.WriteLine(i);
            }
            Console.WriteLine("----");
        }

        private void PrintEnumerable(IEnumerable<IntegerValue> enumerable, string enumerableName)
        {
            Console.WriteLine("----");
            Console.WriteLine("Enumerable name: " + enumerableName);
            foreach (IntegerValue i in enumerable)
            {
                Console.WriteLine(i.GetStringedValue());
            }
            Console.WriteLine("----");
        }

        private IEnumerable<IntegerValue> CreateEnumerable()
        {
            return Enumerable.Range(0, 10).Select(i => new IntegerValue(i));
        }
    }
}