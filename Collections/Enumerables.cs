﻿using Generics;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Collections
{
    public class Enumerables
    {
        public void DoSample()
        {
            IEnumerable<IntegerValue> myEnumerable = this.CreateEnumerable();

            this.PrintEnumerable(myEnumerable, nameof(myEnumerable));

            Console.WriteLine("SELECT");
            IEnumerable<IntegerValue> selectedEnumerable = myEnumerable.Select(it => new IntegerValue(it.Value + 1));

            this.PrintEnumerable(myEnumerable, nameof(myEnumerable));
            this.PrintEnumerable(selectedEnumerable, nameof(selectedEnumerable));

            Console.WriteLine("WHERE");
            IEnumerable<IntegerValue> whereEnumerable = myEnumerable.Where(it => it.Value > 5);

            this.PrintEnumerable(whereEnumerable, nameof(whereEnumerable));

            Console.WriteLine("SUM");
            int sum = myEnumerable.Sum(it => it.Value);
            Console.WriteLine("The sum is: " + sum);

            Console.WriteLine("ZIP");
            string[] secondCollection =
            {
                "first", "second", "third", "fourth", "fifth", "sixth",
                "seventh", "eighth", "ninth", "tenth"
            };

            IEnumerable<string> zippedEnumerable = myEnumerable.Zip(secondCollection,
                (value, s) => $"Item value {value.Value} is the {s}");
            this.PrintEnumerable(zippedEnumerable, nameof(zippedEnumerable));

            Console.WriteLine("AGGREGATE");
            int aggr = myEnumerable.Aggregate(0, (i, value) => i + value.Value);
            Console.WriteLine("The aggregation (sum) is: " + sum);
        }

        private void PrintEnumerable(IEnumerable<string> enumerable, string enumerableName)
        {
            Console.WriteLine("----");
            Console.WriteLine("Enumerable name: " + enumerableName);
            foreach (string i in enumerable)
            {
                Console.WriteLine(i);
            }
            Console.WriteLine("----");
        }

        private void PrintEnumerable(IEnumerable<IntegerValue> enumerable, string enumerableName)
        {
            Console.WriteLine("----");
            Console.WriteLine("Enumerable name: " + enumerableName);
            foreach (IntegerValue i in enumerable)
            {
                Console.WriteLine(i.GetStringedValue());
            }
            Console.WriteLine("----");
        }

        private IEnumerable<IntegerValue> CreateEnumerable()
        {
            return Enumerable.Range(0, 10).Select(i => new IntegerValue(i));
        }
    }
}