﻿using System;

namespace Settings
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            string mySetting = Properties.Settings.Default.MySetting;
            string anotherSetting = Properties.Settings.Default.AnotherSetting;

            Console.WriteLine(mySetting);
            Console.WriteLine(anotherSetting);

            Console.ReadLine();

            Properties.Settings.Default["MySetting"] = "newValue";
            Properties.Settings.Default.Save();

            mySetting = Properties.Settings.Default.MySetting;

            Console.ReadLine();
        }
    }
}
